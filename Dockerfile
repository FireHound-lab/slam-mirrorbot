FROM breakdowns/mega-sdk-python:latest

WORKDIR /usr/src/app
RUN chmod 777 /usr/src/app

COPY . .
RUN chmod +x aria.sh

CMD ["bash","start.sh"]
